$('.ac-header').click(function() {
	let $accordion = $(this).closest('.ac-container');
	$accordion.css('height', $accordion.height() + 'px');

	$accordion.find('.ac-element.active').removeClass('active').delay(100).queue(next => {
		$(this).closest('.ac-element').addClass('active');
	    next();
	});

});